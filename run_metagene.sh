#!/bin/sh

#!/bin/sh

texts=$1
args=("$@") 

if [[ " ${args[*]} " =~ " do_install " ]]; then
bash download_ner_models.sh
wget https://repo.anaconda.com/miniconda/Miniconda3-py37_4.11.0-Linux-x86_64.sh
bash Miniconda3-py37_4.11.0-Linux-x86_64.sh -b
source ~/.bashrc
conda create -n metagene python=3.7 -q
source ~/miniconda3/etc/profile.d/conda.sh
conda activate metagene
pip install -r requirements.txt
fi

if [[ " ${args[*]} " =~ " do_classify " ]]; then
python scripts/classify_texts.py $texts text_classifier/models/random_forest/rf_9_300_25_k5_n100_t0.2_d2_tfidf_all_model.pkl tfidf None 0.4 no text_classifier/output
python scripts/classify_texts.py $texts text_classifier/models/random_forest/rf_9_250_25_k5_n100_t0.2_d2_doc2vec_all_model.pkl doc2vec text_classifier/models/doc2vec/mgnify_doc2vec_200_5.model 0.4 no text_classifier/output
fi

if [[ " ${args[*]} " =~ " do_annotate " ]]; then
python scripts/run_ner_process.py entity_classifier bio-text $texts
bash scripts/run_ner.sh entity_classifier python
python scripts/run_ner_process.py entity_classifier process-ner $texts entity_classifier/output
fi
